package dhbw.web.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.UserService;
import dhbw.web.model.User;
import dhbw.web.model.db.UserDB;
import dhbw.web.model.db.repository.UserDBRepository;
import dhbw.web.model.to.UserTO;
import dhbw.web.utils.MapperWrapper;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/user")
@Slf4j
public class UserController implements UserService {

	@Autowired
	UserDBRepository userDBRepository;

	@Autowired
	private MapperWrapper dbMapper;

	@LoadBalanced
	@PostMapping(path = "")
	@Override
	public User createUser(@RequestBody UserTO user) {
		log.debug("Create: received the object: " + user);
		User u = new User(user.getUsername(), user.getEmail(), user.getFirstname(), user.getLastname());
		u.setId(userDBRepository.save(dbMapper.map(u, UserDB.class)));
		return u;
	}

	@LoadBalanced
	@GetMapping(path = "")
	@Override
	public User[] readAllUser() {
		log.debug("Read all users");
		Iterable<UserDB> udb = userDBRepository.findAll();
		List<User> us = new ArrayList<>();
		for (UserDB userDB : udb) {
			us.add(dbMapper.map(userDB, User.class));
		}
		User[] u = new User[us.size()];
		if (us.size() != 0) {
			u = us.toArray(u);
		}
		return u;
	}

	@LoadBalanced
	@GetMapping(path = "/name/{username}")
	@Override
	public User readUserByUsername(@PathVariable("username") String username) {
		log.debug("Read: received the object: " + username);
		UserDB u = userDBRepository.findByUsername(username);
		if (u == null) {
			return new User();
		}
		return dbMapper.map(u, User.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}")
	@Override
	public User readUser(@PathVariable("id") int id) {
		log.debug("Read: received the object: " + id);
		UserDB u = userDBRepository.findById(id);
		if (u == null) {
			return new User();
		}
		return dbMapper.map(u, User.class);
	}

	@LoadBalanced
	@PutMapping(path = "")
	@Override
	public void updateUser(@RequestBody UserTO user) {
		log.debug("Update: received the object: " + user);

		UserDB db = userDBRepository.findById(user.getId());
		if (db == null) {
			return;
		}

		User u = dbMapper.map(db, User.class);
		u.setEmail(user.getEmail().equals("") ? u.getEmail() : user.getEmail());
		u.setFirstname(user.getFirstname().equals("") ? u.getFirstname() : user.getFirstname());
		u.setLastname(user.getLastname().equals("") ? u.getLastname() : user.getLastname());
		u.setPassword(user.getPassword().equals("") ? u.getPassword() : user.getPassword());
		userDBRepository.save(dbMapper.map(u, UserDB.class));
	}

	@Override
	public void deleteUser(int id) {
		log.debug("Delete: delete user with id: " + id);
		userDBRepository.deleteById(id);
	}

}
