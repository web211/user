package dhbw.web.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import dhbw.web.model.db.mapper.ModelDBMapper;
import dhbw.web.model.db.repository.UserDBRepository;
import dhbw.web.utils.MapperWrapper;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = { "dhbw.web.user", "dhbw.web.model.db.mapper" })
public class UserApplication {

	public static void main(String args[]) {
		SpringApplication.run(UserApplication.class, args);
	}

	@Bean
	public MapperWrapper mapper() {
		return ModelDBMapper.getModelDBMapper();
	}

	@Bean
	public UserDBRepository userRepo() {
		return new UserDBRepository();
	}

}
